{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Modulo sub-footer -->
{if $page_name == 'index'}
    {*<footer id="footer">
        <div class="container">
            <div class="row">
                
            </div>
        </div>
    </footer>*}
{else}
    
{/if}
<section id="pre-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3"><img class="img-responsive" src="{$base_dir}themes/adl_deco/imagenes/productos_garantizados.jpg" /></div>
            <div class="col-md-3"><img class="img-responsive" src="{$base_dir}themes/adl_deco/imagenes/aseguramos_tu_inversion.jpg" /></div>
            <div class="col-md-3"><img class="img-responsive" src="{$base_dir}themes/adl_deco/imagenes/respaldo_confianza.jpg" /></div>
            <div class="col-md-3"><img class="img-responsive" src="{$base_dir}themes/adl_deco/imagenes/garantia.jpg" /></div>
        </div>
    </div>
    <br />
    {if $page_name == 'index'}
    <div class="container">
        <div class="row">
            <div class="col-md-3"><img class="img-responsive" src="{$base_dir}themes/adl_deco/imagenes/logo1.jpg" /></div>
            <div class="col-md-3"><img class="img-responsive" src="{$base_dir}themes/adl_deco/imagenes/logo2.jpg" /></div>
            <div class="col-md-3"><img class="img-responsive" src="{$base_dir}themes/adl_deco/imagenes/logo3.jpg" /></div>
            <div class="col-md-3"><img class="img-responsive" src="{$base_dir}themes/adl_deco/imagenes/logo4.jpg" /></div>
        </div>
    </div>
    <br />
    <br />
    {else}
    <div class="container">
        <div class="row barra-gris">
            <div class="col-sm-12">
                ¿Necesita ayuda? Llámenos: (572) 898 0033 o visítenos Lun-Vie 8AM a 6PM - Sab 8Am a 1PM
            </div>
        </div>
        <div class="row menu-prefooter">
            <div class="col-md-3">
                <div class="titulo menu">Acerca de ADL Decoración</div>
                <ul class="menu sub">
                    <li><a href="#">Quiénes somos</a></li>
                    <li><a href="#">Marcas que representamos</a></li>
                    <li><a href="#">Dónde trabajamos</a></li>
                    <li><a href="#">Trabaje con nosotros</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="titulo menu">Servicio al cliente</div>
                <ul class="menu sub">
                    <li><a href="#">FAQs</a></li>
                    <li><a href="#">¿Cómo tomar medidas?</a></li>
                    <li><a href="#">Manuales y garantías</a></li>
                    <li><a href="#">Bonos de regalo</a></li>
                </ul>
            </div>
            <div class="col-md-6">
                <h4>Recibe ofertas exclusivas y actualizaciones</h4>
                <input type="text" class="caja newsletter" placeholder="Suscríbite a nuestro boletín" />
            </div>
        </div>
    </div>
    {/if}
</section>
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <strong>Sede Sur:</strong> Tel (57)(2) 333 14 23 <br />
                Dir: Calle 16 # 100A - 105 Av. Cañasgordas - Cali <br />
                <strong>Sede Norte:</strong> Tel (57)(2) 668 68 00 <br />
                Dir: Av 6AN # 20N - 04 - Cali <br />
                &copy; 2016 ADL | Todos los derechos reservados
            </div>
            <div class="col-md-3">
                <ul class="redes sociales black">
                    <li><a class="facebook" href="#"></a></li>
                    <li><a class="twitter" href="#"></a></li>
                    <li><a class="gplus" href="#"></a></li>
                    <li><a class="pinterest" href="#"></a></li>
                </ul>
            </div>
            <div class="col-md-2">
                <img src="{$base_dir}themes/adl_deco/imagenes/sello.jpg" class="img-responsive" />
            </div>
        </div>
    </div>
    {*<div class="copy">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center" style="font-size: 1.1rem">
                    2016 &copy; Ancora SM, Todos los derechos reservados
                </div>
            </div>
        </div>
    </div>*}
</footer>