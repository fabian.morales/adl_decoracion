{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- Block user information module HEADER -->
<div class="clearfix"></div>
{*<div class="block_user_carrito">
    <img src="{$base_dir}themes/ancora/imagenes/carrito.png" />
    <span>Mi carrito de compras | 
        {if $cart_qties == 0} 
           $ (0)
        {else} 
            {if $priceDisplay == 1}
                {assign var='blockuser_cart_flag' value='Cart::BOTH_WITHOUT_SHIPPING'|constant}
                {convertPrice price=$cart->getOrderTotal(false, $blockuser_cart_flag)}
            {else}
                {assign var='blockuser_cart_flag' value='Cart::BOTH_WITHOUT_SHIPPING'|constant}
                {convertPrice price=$cart->getOrderTotal(true, $blockuser_cart_flag)}
            {/if}
        {/if}
    </span>
</div>*}
{*<div class="clearfix"></div>
<div class="block_user_login">
    {if $logged}
        <a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="{l s='View my customer account' mod='blockuserinfo'}" class="account" rel="nofollow"><span>{$cookie->customer_firstname} {$cookie->customer_lastname}</span></a> |
        <a href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html'}" title="{l s='Log me out' mod='blockuserinfo'}" class="logout" rel="nofollow">{l s='Sign out' mod='blockuserinfo'}</a>
    {else}
        <a href="{$link->getPageLink('my-account', true)|escape:'html'}" title="{l s='Log in to your customer account' mod='blockuserinfo'}" class="login" rel="nofollow"><i class="icon icon-user"></i> Registro | Iniciar sesión</a>
    {/if}
</div>*}

<div class="row info-top">
    <div class="col-md-3">
        <div class="icono whatsapp">57 (310) 435 76 44</div>
    </div>
    <div class="col-md-3">
        <div class="icono telefono">57 (2) 668 68 00</div>
    </div>
    <div class="col-md-3">
        <div class="icono email">decoracion@adl.co</div>
    </div>
    <div class="col-md-3">
        <img class="img-responsive logo-gris-top" src="{$base_dir}themes/adl_deco/imagenes/logo_gris.png" />
    </div>
</div>
<div class="row info-top">
    <div class="col-md-4">&nbsp;</div>
    <div class="col-md-4" style="margin-bottom: 10px;">
        <input type="text" class="caja buscador" />
        <input type="button" class="boton buscador" value="Buscar" />
        <br />
    </div>
    <div class="col-md-4">
        <a class="link-agenda" href="#">Agende su cita aqu&iacute;</a>
    </div>
</div>

