<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_CAN_LOAD_FILES_'))
    exit;

class MyHome extends Module
{
    public function __construct()
    {
        $this->name = 'myhome';
        if (version_compare(_PS_VERSION_, '1.4.0.0') >= 0)
            $this->tab = 'front_office_features';
        else
            $this->tab = 'Blocks';
        $this->version = '2.1.1';
        $this->author = 'Fabian Morales';

        $this->bootstrap = true;
        parent::__construct();	

        $this->displayName = 'My Home';
        $this->description = 'Bloque HTML para el Home';
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        return parent::install() &&
            $this->registerHook('displayHome') &&
            $this->registerHook('displayHomeTabContent') &&
            $this->registerHook('displayInnerHeader') &&
            $this->registerHook('header');
    }


    public function uninstall()
    {
        // Delete configuration
        parent::uninstall();
    }

    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS(($this->_path).'myhome.css', 'all');
    }

    public function hookDisplayHome($params)
    {
        $categoriasHome = Category::getCategories((int) ($this->context->language->id), true, false, ' and c.level_depth >= 2');

        if (count($categoriasHome) > 8) {
            $categoriasHome = array_slice($categoriasHome, 0, 8);
        }

        $cats = array();
        foreach ($categoriasHome as $c) {
            $cats[] = new Category((int) $c["id_category"], (int) ($this->context->language->id));
        }
        
        $this->smarty->assign('categorias', $cats);

        return $this->display(__FILE__, 'myhome.tpl');
    }
    
    public function hookDisplayHomeTabContent($params)
    {
        return $this->hookDisplayHome($params);
    }
    
    public function hookDisplayInnerHeader($params)
    {
        $home = "0";
        
        if (isset($params["home"])){
            $home = $params["home"];
        }
        $this->smarty->assign('isHome', $home);
        
        $categorias = Category::getCategories((int)($this->context->language->id), true, false, ' and c.level_depth = 2', ' order by category_shop.position asc');
        $this->smarty->assign('categorias', $categorias);
        
        return $this->display(__FILE__, 'innerheader.tpl');
    }

    public function hookDisplayBanner($params){
        return $this->hookDisplayHome($params);
    }
}
