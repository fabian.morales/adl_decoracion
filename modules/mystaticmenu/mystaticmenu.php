<?php

/*
 * 2007-2014 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2014 PrestaShop SA

 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_CAN_LOAD_FILES_'))
    exit;

class MyStaticMenu extends Module {

    public function __construct() {
        $this->name = 'mystaticmenu';
        if (version_compare(_PS_VERSION_, '1.4.0.0') >= 0)
            $this->tab = 'front_office_features';
        else
            $this->tab = 'Blocks';
        $this->version = '2.1.1';
        $this->author = 'Fabian Morales';

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = 'My Static Menu';
        $this->description = 'Bloque de menu estatico';
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install() {
        return parent::install() &&
                $this->registerHook('displayTop') &&
                $this->registerHook('displayInnerLeftColumn') &&
                $this->registerHook('header');
    }

    public function uninstall() {
        // Delete configuration
        parent::uninstall();
    }

    public function hookDisplayHeader($param) {
        $this->context->controller->addCSS($this->_path . 'mystaticmenu.css');
        return "";
    }

    public function hookDisplayTop($params) {
        /*$categoriasMujer = Category::getCategories((int) ($this->context->language->id), true, false, ' and c.id_parent = ' . 12);

        $catsMujer = array();
        foreach ($categoriasMujer as $c) {
            $catsMujer[] = new Category((int) $c["id_category"], (int) ($this->context->language->id));
        }

        $this->smarty->assign('mujer', $catsMujer);*/
        $categorias = Category::getCategories((int) ($this->context->language->id), true, false, ' and c.level_depth = 2', ' order by category_shop.position asc');
        $this->smarty->assign('categorias', $categorias);
        $html = $this->display(__FILE__, 'mystaticmenu.tpl');
        return $html;
    }

    public function hookDisplayInnerLeftColumn($params) {
        $categorias = $this->obtenerCategorias(null);
        
        /*$_attr = AttributeGroup::getAttributesGroups((int)($this->context->language->id));
        $atributos = array();
        $atributosParam = Tools::getValue('atributo');
        foreach ($_attr as $a){
            $atributos[$a["id_attribute_group"]]["atributo"] = $a;
            $atributos[$a["id_attribute_group"]]["valores"] = AttributeGroup::getAttributes((int)($this->context->language->id), (int)$a["id_attribute_group"]);
            
            if (isset($atributosParam[$a["id_attribute_group"]]) && !empty($atributosParam[$a["id_attribute_group"]])){
                $atributos[$a["id_attribute_group"]]["sel"] = $atributosParam[$a["id_attribute_group"]];
            }
        }*/
        
        $this->smarty->assign('categorias', $categorias);
        //$this->smarty->assign('atributos', $atributos);
        
        return $this->display(__FILE__, 'innerLeftColumn.tpl');
    }

    public function obtenerCategorias($padre) {
        $categorias = null;
        $res = array();
        if (!sizeof($padre)) {
            $categorias = Category::getCategories((int) ($this->context->language->id), true, false, ' and c.level_depth = 2', ' order by category_shop.position asc');
        } else {
            $categorias = Category::getChildren($padre["id_category"], (int) ($this->context->language->id), true);
        }

        foreach ($categorias as $c) {
            $c["hijas"] = $this->obtenerCategorias($c);
            $res[] = $c;
        }

        return $res;
    }
}
