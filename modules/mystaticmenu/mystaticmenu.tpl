<div id="top_menu">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="despl-resp hidden-lg hidden-md">
                    <a href="#menu_horz">Menu</a>
                </div>
                
                <div id="menu_horz" class="menu_hrz">
                    <ul class="clearfix menu-resp">                    
                        <li><img src="{$base_dir}themes/adl_deco/imagenes/logo.png" class="fixed-logo" /></li>
                        <li><a href="{$base_dir}index.php" title="Inicio">Inicio</a></li>
                        <li class="padre">
                            <a href="">Productos</a>
                            <div class="submenu">                                
                                <ul>
                                    {foreach from=$categorias item=cat}
                                    <li>
                                        <a href="{$link->getCategoryLink($cat.id_category, $cat.link_rewrite)|escape:'html':'UTF-8'}">
                                            {$cat.name|escape:'html':'UTF-8'}
                                        </a>
                                    </li>
                                    {/foreach}
                                </ul>
                            </div>
                        </li>
                        <li class="padre">
                            <a href="">Tendencias</a>
                            <div class="submenu">
                                <ul>
                                    <li><a href="#">Tendencia 01</a></li>
                                    <li><a href="#">Tendencia 01</a></li>
                                    <li><a href="#">Tendencia 01</a></li>
                                    <li><a href="#">Tendencia 01</a></li>
                                    <li><a href="#">Tendencia 01</a></li>
                                    <li><a href="#">Tendencia 01</a></li>
                                    <li><a href="#">Tendencia 01</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="padre">
                            <a href="">D&oacute;nde comprar</a>
                            <div class="submenu">
                                <ul>
                                    <li><a href="#">Tienda 01</a></li>
                                    <li><a href="#">Tienda 01</a></li>
                                    <li><a href="#">Tienda 01</a></li>
                                    <li><a href="#">Tienda 01</a></li>
                                    <li><a href="#">Tienda 01</a></li>
                                    <li><a href="#">Tienda 01</a></li>
                                    <li><a href="#">Tienda 01</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="padre">
                            <a href="">Ofertas especiales</a>
                            <div class="submenu">
                                <ul>
                                    <li><a href="#">Oferta 01</a></li>
                                    <li><a href="#">Oferta 01</a></li>
                                    <li><a href="#">Oferta 01</a></li>
                                    <li><a href="#">Oferta 01</a></li>
                                    <li><a href="#">Oferta 01</a></li>
                                    <li><a href="#">Oferta 01</a></li>
                                    <li><a href="#">Oferta 01</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>    
</div>


