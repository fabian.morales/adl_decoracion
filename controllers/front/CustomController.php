<?php

/*
 * 2007-2013 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2013 PrestaShop SA
 *  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class CustomControllerCore extends FrontController {

    //public $php_self = 'my_contacto';

    var $destino = "desarrollo@encubo.ws";

    public function setMedia() {
        parent::setMedia();

        if (!$this->useMobileTheme()) {
            //TODO : check why cluetip css is include without js file
            $this->addCSS(array(
                _THEME_CSS_DIR_ . 'product_list.css' => 'all'
            ));
        }

        //$this->addJS(_THEME_JS_DIR_.'category.js');
    }

    /**
     * Initialize product controller
     * @see FrontController::init()
     */
    public function init() {
        parent::init();
    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent() {
        parent::initContent();
        $tarea = Tools::getValue('tarea');

        switch ($tarea) {
            case "subhome":
                $this->mostrarHome2();
                break;
            case "cathome":
                $this->mostrarHomeCategorias();
                break;
            case "producto_ajax":
                $this->mostrarVistaRapidaProducto(Tools::getValue('id_producto'));
                break;
            case "mostrar_contacto":
                $this->mostrarContacto();
                break;
            case "info_comercial":
                $this->mostrarInfoComercial();
                break;
            case "tiendas":
                $this->mostrarTiendas();
                break;
            case "catalogo":
                $this->mostrarCatalogo();
                break;
            case "preguntas_frecuentes":
                $this->mostrarFaqs();
                break;
            case "mostrar_hoja_vida":
                $this->mostrarHojaVida();
                break;
            case "enviar_hoja_vida":
                $this->enviarHojaVida();
                break;
            case "obtener_ciudades":
                $this->obtenerCiudades();
                break;
            case "mostrar_matching_home":
                $this->mostrarMatchingHome();
                break;
            case "mostrar_matching_det":
                $this->mostrarProductosMatching();
                break;
            case "mostrar_historia":
                $this->mostrarHistoria();
                break;
            case "mostrar_mapa":
                $this->mostrarMapa();
                break;
        }
    }

    public function mostrarHome2() {
        $cats = Category::getCategories((int) ($this->context->language->id), true, false, ' and c.id_parent = 21');
        $categorias = array();

        foreach ($cats as $c) {
            $_c = New Category((int) $c["id_category"]);
            $categorias[] = $_c;
        }
        $vars = array("categorias_sil" => $categorias);
        $this->context->smarty->assign($vars);
        $this->setTemplate(_PS_THEME_DIR_ . 'home2.tpl');
    }

    public function mostrarHomeCategorias() {
        //$categoriasHome = Category::getChildren(3, (int)($this->context->language->id), true);
        $parent = Tools::getValue('id_cat');
        $categoriasHome = Category::getCategories((int) ($this->context->language->id), true, false, ' and c.id_parent = ' . $parent);

        if (count($categoriasHome) > 5) {
            $categoriasHome = array_slice($categoriasHome, 0, 5);
        }

        $cats = array();
        foreach ($categoriasHome as $c) {
            $cats[] = new Category((int) $c["id_category"], (int) ($this->context->language->id));
        }

        $vars = array(
            "categorias_menu" => $this->obtenerCategorias(),
            "categorias_home" => $cats
        );

        $this->context->smarty->assign($vars);
        $this->setTemplate(_PS_THEME_DIR_ . 'homecat.tpl');
    }

    public function obtenerCategorias($padre) {
        $categorias = null;
        $res = array();
        if (!sizeof($padre)) {
            $categorias = Category::getCategories((int) ($this->context->language->id), true, false, ' and c.level_depth = 2', ' order by category_shop.position asc');
        } else {
            $categorias = Category::getChildren($padre["id_category"], (int) ($this->context->language->id), true);
        }

        foreach ($categorias as $c) {
            $c["hijas"] = $this->obtenerCategorias($c);
            $res[] = $c;
        }

        return $res;
    }

    public function mostrarVistaRapidaProducto($idProducto) {
        $producto = new Product($idProducto, false, (int) ($this->context->language->id));
        $imagenes = $producto->getImages((int) ($this->context->language->id));
        $vars = array(
            "producto" => $producto,
            "imagenes" => $imagenes
        );
        $this->context->smarty->assign($vars);

        echo $this->context->smarty->fetch(_PS_THEME_DIR_ . 'product-ajax.tpl');
        exit;
    }

    public function mostrarContacto() {
        $this->setTemplate(_PS_THEME_DIR_ . 'contacto.tpl');
    }

    public function mostrarInfoComercial() {
        $this->setTemplate(_PS_THEME_DIR_ . 'info_comercial.tpl');
    }

    public function mostrarTiendas() {
        $this->context->controller->addJS('http://maps.google.com/maps/api/js?sensor=false', 'all');
        $vars["tiendas"] = $this->dataListaUbicaciones();
        $this->context->smarty->assign($vars);
        $this->setTemplate(_PS_THEME_DIR_ . 'tiendas.tpl');
    }

    public function mostrarCatalogo() {
        $this->setTemplate(_PS_THEME_DIR_ . 'catalogo_virtual.tpl');
    }

    public function mostrarFaqs() {
        $this->setTemplate(_PS_THEME_DIR_ . 'faqs.tpl');
    }

    public function mostrarHojaVida() {
        $this->setTemplate(_PS_THEME_DIR_ . 'hoja_vida.tpl');
    }

    public function enviarHojaVida() {
        $archivo = null;
        $f = $_FILES["archivo"];
        $info = null;
        if (is_array($f) && is_uploaded_file($f["tmp_name"]) && !$f['error']) {
            $info = pathinfo($f['name']);
            $mime = array(
                "doc" => "applicacion/msword",
                "docx" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                "xls" => "application/vnd.ms-excel",
                "xlsx" => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                "pdf" => "applicacion/pdf",
                "rtf" => "applicacion/rtf",
                "odt" => "application/odt",
                "ods" => "application/vnd.oasis.opendocument.spreadsheet"
            );

            if (array_key_exists($info['extension'], $mime)) {
                $archivo = array("content" => file_get_contents($f['tmp_name']), "mime" => $mime[$info['extension']], "name" => "hoja_de_vida_" . urlencode(Tools::getValue("nombre")) . "." . $info['extension']);
            }
        }

        $emailVars = array(
            "{nombre}" => Tools::getValue("nombre"),
            "{telefono}" => Tools::getValue("telefono"),
            "{email}" => Tools::getValue("email")
        );

        Mail::Send($this->context->language->id, 'hojavida', 'Hoja de vida enviada', $emailVars, $this->destino, null, null, null, array($archivo), null, _PS_MAIL_DIR_, false, $this->context->shop->id
        );

        $vars["respuesta_tarea"] = "Su hoja de vida ha sido enviada.";
        $this->context->smarty->assign($vars);
        $this->setTemplate(_PS_THEME_DIR_ . 'hoja_vida.tpl');
    }

    public function obtenerCiudades() {
        $idDepto = Tools::getValue("id_depto");
        echo json_encode($this->dataListaCiudad($idDepto));
        die();
    }
    
    public function mostrarMatchingHome(){
        $catMatching = new Category(22, (int) ($this->context->language->id));
        $catsMatching = Category::getChildren($catMatching->id, (int)($this->context->language->id), true);
        $_catsMatching = array();
        
        foreach ($catsMatching as $c){
            $_catsMatching[] = new Category((int)$c["id_category"], (int)($this->context->language->id));
        }
        
        $vars["catMatching"] = $catMatching;
        $vars["catsMatching"] = $_catsMatching;
        $this->context->smarty->assign($vars);
        $this->setTemplate(_PS_THEME_DIR_ . 'matching/inicio.tpl');
    }
    
    public function mostrarProductosMatching(){
        ini_set('display_erros', 1);
        $idCat = Tools::getValue("id");
        $catMatching = new Category($idCat, (int)($this->context->language->id));
        $catsMatching = Category::getChildren($catMatching->id, (int)($this->context->language->id), true);
        $productos = array();
        
        foreach($catsMatching as $c){
            $cat = new Category((int)$c["id_category"], (int)($this->context->language->id));
            $productos[] = $cat->getProducts((int)($this->context->language->id), null, null, null, null, false, true, true, 2);
        }
        
        $vars["catMatching"] = $catMatching;
        $vars["catsMatching"] = $catsMatching;
        $vars["productos"] = $productos;
        $this->context->smarty->assign($vars);
        
        $this->setTemplate(_PS_THEME_DIR_ . 'matching/detalle.tpl');
    }
    
    public function mostrarMapa(){
        $this->setTemplate(_PS_THEME_DIR_ . 'mapa.tpl');
    }

    public function dataListaCiudad($idDepto) {
        $db = Db::getInstance();
        $sql = "select * from " . _DB_PREFIX_ . "ciudad where id_depto = " . $idDepto;
        return $db->executeS($sql);
    }

    public function dataListaUbicaciones() {
        $db = Db::getInstance();
        $sql = "select * from " . _DB_PREFIX_ . "ubicacion_tienda";
        return $db->executeS($sql);
    }
    
    public function mostrarHistoria(){
        $catMatching = new Category(25, (int) ($this->context->language->id));
        $catsMatching = Category::getChildren($catMatching->id, (int)($this->context->language->id), true);
        $_catsMatching = array();
        
        foreach ($catsMatching as $c){
            $_catsMatching[] = new Category((int)$c["id_category"], (int)($this->context->language->id));
        }
        
        $vars["catMatching"] = $catMatching;
        $vars["catsMatching"] = $_catsMatching;
        $this->context->smarty->assign($vars);
        $this->setTemplate(_PS_THEME_DIR_ . 'matching/historia.tpl');
    }
}
