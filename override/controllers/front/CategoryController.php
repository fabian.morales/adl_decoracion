<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class CategoryController extends CategoryControllerCore
{
    /**
     * Assign list of products template vars
     */
    public function assignProductList()
    {
        $hookExecuted = false;
        Hook::exec('actionProductListOverride', array(
            'nbProducts' => &$this->nbProducts,
            'catProducts' => &$this->cat_products,
            'hookExecuted' => &$hookExecuted,
        ));

        // The hook was not executed, standard working
        if (!$hookExecuted)
        {
            $atributos = Tools::getValue('atributo');
            
            $this->context->smarty->assign('categoryNameComplement', '');
            
            if (!empty($atributos)){
                $this->nbProducts = $this->category->getProductsExt(null, $atributos, null, null, $this->orderBy, $this->orderWay, true);
                $this->pagination((int)$this->nbProducts); // Pagination must be call after "getProducts"
                $this->cat_products = $this->category->getProductsExt($this->context->language->id, $atributos, (int)$this->p, (int)$this->n, $this->orderBy, $this->orderWay);
            }
            else{
                $this->nbProducts = $this->category->getProducts(null, null, null, $this->orderBy, $this->orderWay, true);
                $this->pagination((int)$this->nbProducts); // Pagination must be call after "getProducts"
                $this->cat_products = $this->category->getProducts($this->context->language->id, (int)$this->p, (int)$this->n, $this->orderBy, $this->orderWay);
            }
        }
        // Hook executed, use the override
        else
            // Pagination must be call after "getProducts"
            $this->pagination($this->nbProducts);

        Hook::exec('actionProductListModifier', array(
            'nb_products' => &$this->nbProducts,
            'cat_products' => &$this->cat_products,
        ));

        foreach ($this->cat_products as &$product)
            if (isset($product['id_product_attribute']) && $product['id_product_attribute'] && isset($product['product_attribute_minimal_quantity']))
                $product['minimal_quantity'] = $product['product_attribute_minimal_quantity'];

        $this->addColorsToProductList($this->cat_products);

        $this->context->smarty->assign('nb_products', $this->nbProducts);
    }
    
    /**
     * Initializes page content variables
     */
    public function initContent()
    {
        parent::initContent();

        $this->setTemplate(_PS_THEME_DIR_.'category.tpl');

        if (!$this->customer_access) {
            return;
        }

        if (isset($this->context->cookie->id_compare)) {
            $this->context->smarty->assign('compareProducts', CompareProduct::getCompareProducts((int)$this->context->cookie->id_compare));
        }

        // Product sort must be called before assignProductList()
        $this->productSort();

        $this->assignScenes();
        $this->assignSubcategories();
        $this->assignProductList();
        $categorias = $this->obtenerCategorias(null);
        
        $_attr = AttributeGroup::getAttributesGroups((int)($this->context->language->id));
        $atributos = array();
        $atributosParam = Tools::getValue('atributo');
        foreach ($_attr as $a){
            $atributos[$a["id_attribute_group"]]["atributo"] = $a;
            $atributos[$a["id_attribute_group"]]["valores"] = AttributeGroup::getAttributes((int)($this->context->language->id), (int)$a["id_attribute_group"]);
            
            if (isset($atributosParam[$a["id_attribute_group"]]) && !empty($atributosParam[$a["id_attribute_group"]])){
                $atributos[$a["id_attribute_group"]]["sel"] = $atributosParam[$a["id_attribute_group"]];
            }
        }
        
        if (sizeof($this->cat_products)){
            $_productos = array();
            foreach ($this->cat_products as $p){
                $images = Image::getImages((int)($this->context->language->id), (int)$p["id_product"]);
                //print_r($images);                
                if (sizeof($images)){
                    if (count($images) > 1){
                        $p["sec_image"] = $images[1]["id_image"];
                    }
                    else{
                        $p["sec_image"] = $images[0]["id_image"];
                    }
                }
                
                $_productos[] = $p;
            }
            
            $this->cat_products = $_productos;
        }

        $this->context->smarty->assign(array(
            'category'             => $this->category,
            'description_short'    => Tools::truncateString($this->category->description, 350),
            'products'             => (isset($this->cat_products) && $this->cat_products) ? $this->cat_products : null,
            'id_category'          => (int)$this->category->id,
            'id_category_parent'   => (int)$this->category->id_parent,
            'return_category_name' => Tools::safeOutput($this->category->name),
            'path'                 => Tools::getPath($this->category->id),
            'add_prod_display'     => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
            'categorySize'         => Image::getSize(ImageType::getFormatedName('category')),
            'mediumSize'           => Image::getSize(ImageType::getFormatedName('medium')),
            'thumbSceneSize'       => Image::getSize(ImageType::getFormatedName('m_scene')),
            'homeSize'             => Image::getSize(ImageType::getFormatedName('home')),
            'allow_oosp'           => (int)Configuration::get('PS_ORDER_OUT_OF_STOCK'),
            'comparator_max_item'  => (int)Configuration::get('PS_COMPARATOR_MAX_ITEM'),
            'suppliers'            => Supplier::getSuppliers(),
            'body_classes'         => array($this->php_self.'-'.$this->category->id, $this->php_self.'-'.$this->category->link_rewrite),
            'categorias' => $categorias,
            'atributos' => $atributos
        ));
    }
    
    public function obtenerCategorias($padre){            
        $categorias = null;
        $res = array();
        if (!sizeof($padre)){
            $categorias = Category::getCategories((int)($this->context->language->id), true, false, ' and c.level_depth = 2', ' order by category_shop.position asc');
        }
        else{
            $categorias = Category::getChildren($padre["id_category"], (int)($this->context->language->id), true);
        }

        foreach ($categorias as $c){            
            $c["hijas"] = $this->obtenerCategorias($c);
            $res[] = $c;
        }

        return $res;
    }
}
