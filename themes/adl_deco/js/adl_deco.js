(function ($, window) {
    function ajustarVisibilidadMenu(){		
        var docViewTop = $(window).scrollTop();
        var docViewBottom = docViewTop + ($(window).height());
        var elemTop = $("#top_menu").offset().top;
        var elemBottom = elemTop + $("#top_menu").height();
        
        if (elemTop <= docViewTop && docViewTop > 0){                        
            $("#top_menu").addClass("menu-fijo");
        }
        else{
            $("#top_menu").removeClass("menu-fijo");        
        }
    }
    
    $(document).ready(function () {       
       
        $(".despl-resp a").click(function(e) {
            e.preventDefault();
            var $target = $(this).attr("href");
            $($target).toggle();
        });
        
        $(window).scroll(function () {
            ajustarVisibilidadMenu();
        });
        
        $(window).resize(function() {
            $(".despl-resp a").each(function(i, o) {
                var $target = $(o).attr("href");
                if (!$(o).is(":visible")){
                    $($target).show();
                }
                else{
                    $($target).hide();
                }
            });
        });
        
        ajustarVisibilidadMenu();
    });
})(jQuery, window);
